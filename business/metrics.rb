# frozen_string_literal: true

class Metrics
  def self.analyse!(data)
    new(data).analyse
  end

  def initialize(data)
    @data = data
    @result = { unique_view: {}, general_view: {} }
  end

  def analyse
    return if data.nil?

    data.each_key do |k|
      fill_result_hash(:unique_view, data[k].count, k)
      fill_result_hash(:general_view, data[k].sum(&:count), k)
    end

    result
  end

  private

  attr_reader :data, :result

  def fill_result_hash(type, key, value)
    result[type][key] ||= []
    result[type][key] << value
  end
end
