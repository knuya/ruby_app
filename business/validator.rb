# frozen_string_literal: true

class Validator
  class << self
    def validate(file)
      new(file).validate
    end

    def valid_line(line)
      line.start_with?('/') &&
        line.split(' ').size == 2
    end
  end

  def initialize(file)
    @file = file
    @errors = []
  end

  def validate
    !blank_file && valid_file

    return errors unless errors.empty?
  end

  private

  attr_reader :errors, :file

  def blank_file
    errors << 'file not informed' if file.nil?
  end

  def valid_file
    file_response = File.open(file)

    errors << 'malformed line' unless self.class.valid_line(file_response.readline)
  rescue Errno::ENOENT => _
    errors << 'file not valid'
  end
end
