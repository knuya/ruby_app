# frozen_string_literal: true

require 'logger'

class Processor
  LOG_FILE = 'logs/malformed_lines.log'

  def self.run!(file)
    new(file).run
  end

  def initialize(file)
    @file = file
    @result = {}
  end

  def run
    file.each_line(chomp: true) do |line|
      @line = line

      process_line if check_line
    end

    result
  end

  private

  attr_reader :file, :line, :result

  def process_line
    check_result

    result
  end

  def check_line
    return true if Validator.valid_line(line)

    Logger.new(LOG_FILE, "filename: #{file.path} with `#{line}`")

    false
  end

  def check_result
    @line = line.split(' ')
    data = result[line[0]]
    return result[line[0]] = [create_data] if data.nil?

    increment_data(data)
  end

  def create_data
    access_data.new(line[0], line[1], 1)
  end

  def increment_data(data)
    element = data.detect { |access| access.ip == line[1] }
    data << create_data && return if element.nil?

    element.count += 1
  end

  def access_data
    @access_data ||= Struct.new(:website, :ip, :count)
  end
end
