# frozen_string_literal: true

require 'spec_helper'
require_relative '../../business/processor'

RSpec.describe Processor do
  describe '.run!' do
    subject { described_class.run!(file) }

    context 'with valid file' do
      let(:file) { File.open('spec/fixtures/valid_file.log') }

      before { expect(Logger).to_not receive(:new) }

      it { is_expected.to be_truthy }
    end

    context 'with a valid file with invalid lines' do
      let(:file_path) { 'spec/fixtures/valid_file_with_invalid_lines.log' }
      let(:file) { File.open(file_path) }
      before do
        expect(Logger).to receive(:new).with('logs/malformed_lines.log', "filename: #{file_path} with `invalid line`")
        expect(Logger).to receive(:new).with('logs/malformed_lines.log', "filename: #{file_path} with `other`")
      end

      it { is_expected.to be_truthy }
    end
  end
end
