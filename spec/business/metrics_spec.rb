# frozen_string_literal: true

require 'spec_helper'
require_relative '../../business/metrics'

RSpec.describe Metrics do
  describe '.analyse!' do
    let(:file) { File.open('spec/fixtures/valid_file.log') }

    let(:data) { Processor.run!(file) }
    subject { described_class.analyse!(data) }

    context 'when receive a valid data' do
      it { is_expected.to be_a Hash }
    end

    context 'when receive no data' do
      let(:data) {}

      it { is_expected.to be_nil }
    end
  end
end
