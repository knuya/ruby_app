# frozen_string_literal: true

require 'spec_helper'
require_relative '../../business/validator'

RSpec.describe Validator do
  let(:line) { '/home 1' }
  let(:file) { 'a_file' }
  let(:file_response) { double(:file, readline: line) }

  before { allow(File).to receive(:open).and_return(file_response) }

  describe '.validate' do
    subject { described_class.validate(file) }

    context 'when have a file' do

      it { is_expected.to be_nil }

      context 'a invalid input' do
        let(:line) { '' }

        it { is_expected.to eq ['malformed line'] }
      end
    end

    context 'when the file is nil' do
      let(:file) {}

      it { is_expected.to eq ['file not informed'] }
    end
  end

  describe '.valid_line' do
    subject { described_class.valid_line(line) }

    context 'with a valid line' do
      it { is_expected.to be_truthy }
    end

    context 'with invalid line' do
      let(:line) { 'invalid' }

      it { is_expected.to be_falsey }
    end
  end
end
