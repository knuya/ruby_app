# frozen_string_literal: true

require 'spec_helper'
require_relative '../../bin/parser'

RSpec.describe Parser do
  let(:file) { 'a_file' }
  let(:file_response) { double(:file, readline: '/home 1') }
  let(:validator_response) {}


  before do
    allow(File).to receive(:open).and_return(file_response)
    allow_any_instance_of(Validator).to receive(:validate).and_return(validator_response)
    ARGV ||= [file].freeze
  end

  describe '#new' do
    subject { -> { described_class.new } }

    context 'when have a file' do
      it { is_expected.to_not raise_error }
    end

    context 'when the file is nil' do
      let(:file) {}
      let(:validator_response) { ['file not informed'] }

      it { is_expected.to raise_error(StandardError, 'file not informed') }
    end
  end

  describe '#process!' do
    subject { described_class.new.process! }

    before do
      expect(Processor).to receive(:run!).with(file_response).and_return(true)
      expect(Metrics).to receive(:analyse!)
      expect(Presenter::Result).to receive(:show)
    end

    it { is_expected.to be_nil }
  end
end
