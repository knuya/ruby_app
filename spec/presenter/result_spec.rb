# frozen_string_literal: true

require 'spec_helper'
require_relative '../../presenter/result'

RSpec.describe Presenter::Result do
  let(:data) do
    {
      unique_view: {
        8 => ['/home', '/app'],
        7 => ['/test'],
        10 => ['/about']
      }
    }
  end
  describe '.show' do
    subject { described_class.show(data) }

    it 'expect to do the right output' do
      expect($stdout).to receive(:puts).with('/about 10 unique views')
      expect($stdout).to receive(:puts).with('/home 8 unique views')
      expect($stdout).to receive(:puts).with('/app 8 unique views')
      expect($stdout).to receive(:puts).with('/test 7 unique views')

      subject
    end
  end
end
