## Description
I'm using ruby 2.7.0 to do the script

## How to install

You have to use ruby 2.7.0
make sure you have bundler installed, if not just run `gem install bundler`

`bundle install`

## How to execute

`bin/parser.rb path_to_file`

If you have some trouble to execute please execute `chmod +x bin/parser.rb`
## Output from the example
```
/help_page/1 23 unique views
/contact 23 unique views
/home 23 unique views
/index 23 unique views
/about/2 22 unique views
/about 21 unique views
```

## How to run tests

`bundle exec rake spec`

## Project structure

- The parser.rb to call like the proposal
- A spec folder for tests
- logs folder, with all relevant files (only rubocop.log for now)

## Tests

Using `rspec` to do the tests and `simplecov` to check the coverage.

## To improve

- Make a model for the metrics
- Produce a better tests and make a load test
- A better analysis on th Big O of the algorithm to make a better complexity
- put the order responsability in the metrics and not let for presenter do it
- better errors control, all the erros in the project are producing standard error
- I was preparing to output the general views as well, but I not put this to work, only prepare the hash
- produce more log files, preparing to join into a datadog or something similar

## Coverage

`Coverage report generated for RSpec to /Users/luiz.camargo/Projects/parser/coverage. 176 / 177 LOC (99.44%) covered.`
