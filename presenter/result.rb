# frozen_string_literal: true

module Presenter
  class Result
    def self.show(data)
      new(data).output
    end

    def initialize(data)
      @data = data
    end

    def output
      ordered_keys.each do |k|
        data_by_key(k).each do |web|
          puts "#{web} #{k} unique views"
        end
      end
    end

    private

    attr_reader :data

    def ordered_keys
      data[:unique_view].keys.sort_by(&:-@)
    end

    def data_by_key(key)
      data.dig(:unique_view, key)
    end
  end
end
