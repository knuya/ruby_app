#!/usr/bin/env ruby
# frozen_string_literal: true

require './business/validator'
require './business/processor'
require './business/metrics'
require './presenter/result'

class Parser
  def initialize
    @file = ARGV[0]

    validate
  end

  def process!
    process
    generate_metrics
    show_data
  end

  private

  attr_reader :file, :processed, :data

  def validate
    errors = Validator.validate(file)

    raise errors.join(', ') unless errors.nil?
  end

  def process
    @processed = Processor.run!(File.open(file))
  end

  def generate_metrics
    @data = Metrics.analyse!(processed)
  end

  def show_data
    Presenter::Result.show(data)
  end
end

Parser.new.process! unless ARGV[0].start_with?('--')
